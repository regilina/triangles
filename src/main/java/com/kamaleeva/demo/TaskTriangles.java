package com.kamaleeva.demo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class TaskTriangles {
    private static double maxArea = -1;
    private static Triangle maxTriangle;
    public static void main(String[] args) {
        try(Stream<String> stream = Files.lines(Paths.get("src/main/resources/" + args[0]), Charset.forName("UTF-8"))) {     
            stream.forEach(string -> calculate(string));
        } catch(IndexOutOfBoundsException | IOException e){  
            e.printStackTrace();
        }  
        try(BufferedWriter writer = Files.newBufferedWriter(Paths.get("src/main/resources/" + args[1]), Charset.forName("UTF-8"))){
            if(null != maxTriangle) {
                writer.write(maxTriangle.toString());
            }
        } catch(IndexOutOfBoundsException | IOException ex){
            ex.printStackTrace();
        }        
     }

    private static String[] coordinates;
    private static Triangle triangle;
    private static double area;
    
    private static void calculate(String string) {
        coordinates = string.split(" ");
        try {
            int x1 = Integer.parseInt(coordinates[0]);
            int y1 = Integer.parseInt(coordinates[1]);
            int x2 = Integer.parseInt(coordinates[2]);
            int y2 = Integer.parseInt(coordinates[3]);
            int x3 = Integer.parseInt(coordinates[4]);
            int y3 = Integer.parseInt(coordinates[5]);
        
            triangle = new Triangle(x1, y1, x2, y2, x3, y3);
     
            if(triangle.isIsosceles()) {
                area = triangle.calculateArea();
                if(maxArea < area) {
                    maxArea = area;
                    maxTriangle = triangle;
                }            
            }
        } catch(NumberFormatException | IndexOutOfBoundsException ex) {
            ex.getMessage();
        }  
    }
}