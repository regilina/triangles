package com.kamaleeva.demo;

public class Triangle {
    private int xA;
    private int yA;
    private int xB;
    private int yB;
    private int xC;
    private int yC;   
 
    public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        xA = x1;
        yA = y1;
        xB = x2;
        yB = y2;
        xC = x3;
        yC = y3;
    }

    public boolean isIsosceles() {
        double sideLength1 = Math.sqrt(Math.pow((xB - xA), 2) + Math.pow((yB - yA), 2));
        double sideLength2 = Math.sqrt(Math.pow((xC - xA), 2) + Math.pow((yC - yA), 2));
        double sideLength3 = Math.sqrt(Math.pow((xC - xB), 2) + Math.pow((yC - yB), 2));
        return sideLength1 == sideLength2 || sideLength1 == sideLength3 || sideLength2 == sideLength3;
    }

    public double calculateArea() {
        double temp = (xB - xA) * (yC - yA) - (xC - xA) * (yB - yA);
        double area = Math.abs(temp) / 2;
        return area;
    }

    @Override
    public String toString() {
        return xA + " " + yA + " " + xB + " " + yB + " " + xC + " " + yC + '\n';
    }
}


