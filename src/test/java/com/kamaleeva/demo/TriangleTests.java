package com.kamaleeva.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TriangleTests {
    private final Triangle degenerateTriangle = new Triangle(50, 50, 50, 50, 50, 50);
    private final Triangle usualTriangle = new Triangle(0, 4, 4, 0, 0, 0);

    @Test
    public void calculateAreaForDegenerateTriangleShouldReturnZero() {
        assertTrue(degenerateTriangle.calculateArea() == 0); 
    }

    @Test
    public void calculateAreaForUsualTriangleShouldReturnExpected() {
        int expected = 8;
        assertTrue(usualTriangle.calculateArea() == expected);
    }
    
    private final Triangle isoscelesTriangle = new Triangle(0, 3, 3, 0, 0, 0);
    private final Triangle notIsoscelesTriangle = new Triangle(0, 1, 2, 0, 0, 0);

    @Test
    public void isIsoscelesForIsoscelesTriangleShouldReturnTrue() {
        assertTrue(isoscelesTriangle.isIsosceles()); 
    }

    @Test
    public void isIsoscelesForNotIsoscelesTriangleShouldReturnFalse() {
        assertFalse(notIsoscelesTriangle.isIsosceles());
    }
}